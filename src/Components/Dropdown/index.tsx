import React from 'react';
import styles from './Dropdown.module.css';

interface Item {
    key: number;
    text: string;
}

interface DropdownProps {
    label: string;
    selected: Item['key'];
    onChange: (key: Item['key']) => void;
    options: Item[];
}

function Dropdown(props: DropdownProps) {
    const id = `dropdown-` + Math.round(Math.random() * 100000);

    return (
        <React.Fragment>
            <label htmlFor={id} className={styles.label}>{props.label}</label>
            <select id={id} className={styles.select} value={props.selected} onChange={e => props.onChange(parseInt(e.currentTarget.value))}>
                {props.options.map(option => <option key={option.key} value={option.key}>{option.text}</option>)}
            </select>
        </React.Fragment>
    );

}

export default Dropdown;
