import React from 'react';
import styles from './Title.module.css';

interface TitleProps {
    size?: 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'h6';
    hasMargin?: boolean;
    className?: string;
    style?: React.CSSProperties;
    children?: React.ReactNode;
}

const getClassName = (props: TitleProps) => {
    return [
        styles.title,
        styles[props.size || 'h1'],
        typeof props.hasMargin === 'boolean' && props.hasMargin === true ? '' : styles.noMargin,
        props.className
    ].filter(s => !!s).join(' ');
}

function Title(props: TitleProps) {
    return <div className={getClassName(props)} style={props.style}>{props.children}</div>;
}

export default Title;
