import React, { useEffect, useRef, useState } from 'react';
import styles from './Loader.module.css';

interface LoaderProps {
    completed?: boolean;
    percentagePerSecond?: number;
    updatesPerSecond?: number;
}

function Loader(props: LoaderProps) {
    const [ progress, setProgress ] = useState(props.completed ? 100 : 0);

    // Attach will unmount event to set current HTML to false to know whether to update progress state variable
    const componentIsMounted = useRef(true);
    useEffect(() => (() => { componentIsMounted.current = false }), []);


    const percentagePerSecond = props.percentagePerSecond || 100;
    const updatesPerSecond = props.updatesPerSecond || 10;


    if (progress < 100) {
        setTimeout(() => {
            if (componentIsMounted.current)
                setProgress(progress + percentagePerSecond / updatesPerSecond)
        }, 1000 / updatesPerSecond);
    }

    return (
        <div className={styles.loader}>
            <div className={styles.bar} style={{ width: `${progress}%` }}></div>
        </div>
    );
}

export default Loader;
