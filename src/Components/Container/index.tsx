import React, { MouseEvent } from 'react';
import styles from './Container.module.css';

interface ContainerProps {
    id?: string;
    boxShadow?: number;
    children?: React.ReactNode;
    style?: React.CSSProperties;
    className?: string;
    onClick?: (event: React.MouseEvent<HTMLDivElement, globalThis.MouseEvent>) => void;
}

function Container(props: ContainerProps) {
    let classNames = [styles.container];

    if (typeof props.boxShadow === 'number') {
        classNames.push(`box-shadow box-${props.boxShadow}`);
    } else {
        classNames.push(styles.simple);
    }

    if (props.className) classNames.push(props.className);

    return (
        <div id={props.id} className={classNames.join(' ')} style={props.style} onClick={props.onClick}>
            {props.children}
        </div>
    );
}

export default Container;
