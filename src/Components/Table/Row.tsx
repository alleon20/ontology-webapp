import React from 'react';
import Button from '../Button';

interface TableRowProps {
    onSelect: (term: OntologyTerm) => void;
    term: OntologyTerm;
    textCutoff?: number;
    className?: string;
}

function TableRow(props: TableRowProps) {
    let desc = props.term.description?.join(' ') || 'No description.';

    if (props.textCutoff && desc.length > props.textCutoff) {
        desc = desc.substr(0, props.textCutoff) + '...';
    }

    return (
        <tr key={props.term.obo_id}>
            <td className={props.className}>{props.term.label}</td>
            <td className={props.className}>{props.term.obo_id}</td>
            <td className={props.className}>{desc}</td>
            <td className={props.className}><Button onClick={() => props.onSelect(props.term)}>View&nbsp;More</Button></td>
        </tr>
    );
}

export default TableRow;
