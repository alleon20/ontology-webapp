import React from 'react';
import Button from '../Button';

interface TablePaginationProps {
    number: number;
    total: number;
    className?: string;
    first: () => void;
    prev: () => void;
    next: () => void;
    last: () => void;
}

function TablePagination(props: TablePaginationProps) {
    return (
        <div className={props.className}>
            <Button round disabled={props.number === 0} onClick={props.first}>&#8810;</Button>
            <Button round disabled={props.number === 0} onClick={props.prev}>&#9664;</Button>
            <div style={{ padding: '0 1rem', lineHeight: '2.2rem' }}>
                Viewing page: {props.number + 1} of {props.total + 1}
            </div>
            <Button round disabled={props.number === props.total} onClick={props.next}>&#9654;</Button>
            <Button round disabled={props.number === props.total} onClick={props.last}>&#8811;</Button>
        </div>
    );
}

export default TablePagination;
