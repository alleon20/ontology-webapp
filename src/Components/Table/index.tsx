import React, { Component } from 'react';
import Button from '../Button';
import Container from '../Container';
import Title from '../Title';
import Input from '../Input';
import styles from './Table.module.css';
import Dropdown from '../Dropdown';
import TableRow from './Row';
import Loader from '../Loader';
import Modal from '../Modal';
import TablePagination from './Pagination';

interface TableProps {
    title: string;
    srcUrl: string;
}

interface TableState {
    data: OntologyTerm[];
    pageSettings: OntologyTermsResponse['page'] | null;
    searchText: string;
    isLoading: boolean;
    selected: OntologyTerm | null;
    modalId: string;
}


class Table extends Component<TableProps, TableState> {

    timeout?: number;
    textCutoff = 40;
    requestNumber = 0;
    pageOptions = [ 5, 10, 20, 50, 100 ];

    constructor(props: TableProps) {
        super(props);
        this.state = {
            data: [],
            pageSettings: null,
            searchText: '',
            isLoading: true,
            selected: null,
            modalId: 'modal-' + Math.round(Math.random() * 100000)
        };
    }

    render() {

        if (!this.state.pageSettings) {
            return (
                <Container boxShadow={2}>
                    <Loader />
                    <Title size="h2">Loading</Title>
                </Container>
            );
        }

        const data = !this.state.searchText
            ? this.state.data
            : this.state.data.filter(row => JSON.stringify(row).toLowerCase().includes(this.state.searchText));

        const ps = this.state.pageSettings;


        const filters = (
            <Container boxShadow={4} className={styles.filterContainer}>
                <div className={styles.filterSearch}>
                    <Input
                        placeholder="Filter current"
                        value={this.state.searchText}
                        onChange={searchText => this.setState({ searchText })} />
                </div>
                <TablePagination
                    className={styles.filterPages}
                    number={ps.number}
                    total={ps.totalPages - 1}
                    first={() => this.viewFirst()}
                    prev={() => this.viewPrev()}
                    next={() => this.viewNext()}
                    last={() => this.viewLast()} />
                <div className={styles.filterDropdown}>
                    <Dropdown
                        label="Page size"
                        selected={ps.size || 20}
                        onChange={size => this.changePageSize(size)}
                        options={this.pageOptions.map(n => ({ key: n, text: `${n}` }))} />
                </div>
            </Container>
        );


        return (
            <React.Fragment>
                <Modal
                    id={this.state.modalId}
                    onClose={() => this.setState({ selected: null })}
                    term={this.state.selected} />
                <Container boxShadow={2}>
                    <Title size="h2">{this.props.title}</Title>
                </Container>
                {filters}
                <Container boxShadow={2}>
                    {this.state.isLoading ? (
                        <Loader completed={!this.state.isLoading} percentagePerSecond={this.state.pageSettings.size <= 20 ? 250 : 150} />
                    ) : ''}
                    <table className={styles.table}>
                        <thead>
                            <tr>
                                <th className={styles.th + ' ' + styles.tdlabel}>Label</th>
                                <th className={styles.th}>Id</th>
                                <th className={styles.th}>Description</th>
                                <th className={styles.th}>Details</th>
                            </tr>
                        </thead>
                        <tbody>
                            {data.length > 0
                                ? data.map(row =>
                                    <TableRow
                                        key={row.obo_id}
                                        onSelect={term => this.setState({ selected: term })}
                                        term={row}
                                        textCutoff={this.textCutoff}
                                        className={styles.td} />
                                ) : (
                                    <tr>
                                        <td colSpan={4} className={styles.td} style={{ textAlign: 'center' }}>No data found</td>
                                    </tr>
                                )}
                        </tbody>
                    </table>
                </Container>
            </React.Fragment>
        );
    }

    componentDidMount() {
        this.getData();
    }

    getData() {
        const url = new URL(this.props.srcUrl);

        this.requestNumber++;
        const requestNumber = this.requestNumber;

        url.searchParams.append('page', (this.state.pageSettings?.number || 0).toString());
        url.searchParams.append('size', (this.state.pageSettings?.size || 20).toString());


        if (!this.state.isLoading) {
            this.setState({ isLoading: true });
        }


        fetch(url.toString())
            .then(res => res.json())
            .then((res: OntologyTermsResponse) => {
                if (this.requestNumber !== requestNumber) return;
                this.setState({
                    data: res._embedded?.terms || [],
                    pageSettings: res.page,
                    isLoading: false
                })
            })
            .catch(e => {
                console.error(e);
                this.setState({ isLoading: false });
            });
    }

    changePageSize(size: number) {
        const pageSettings = this.state.pageSettings;

        this.setState({
            pageSettings: {
                number: 0,
                size,
                totalElements: pageSettings?.totalElements || 0,
                totalPages: pageSettings?.totalPages || 0
            }
        }, () => this.getData());
    }


    alterViewPage(fn: (n: number) => number) {
        if (!this.state.pageSettings) return;

        let { number, size, totalElements, totalPages } = this.state.pageSettings;

        const newNumber = fn(number);

        if (newNumber >= totalPages) return;
        if (newNumber <= -1) return;

        this.setState({ pageSettings: { number: newNumber, size, totalElements, totalPages } }, () => this.getData());
    }


    viewNext() {
        this.alterViewPage(n => n + 1);
    }

    viewPrev() {
        this.alterViewPage(n => n - 1);
    }

    viewFirst() {
        this.alterViewPage(n => 0);
    }

    viewLast() {
        if (!this.state.pageSettings) return;
        const { totalPages } = this.state.pageSettings;

        this.alterViewPage(n => totalPages - 1);
    }

}

export default Table;
