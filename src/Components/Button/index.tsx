import React from 'react';
import styles from './Button.module.css';

interface ButtonProps {
    type?: 'primary' | 'danger';
    round?: boolean;
    disabled?: boolean;
    style?: React.CSSProperties;
    onClick?: () => void;
    className?: string;
    children?: React.ReactNode;
}

function Button(props: ButtonProps) {
    const classNames = [styles.button];

    if (props.type && props.type !== 'primary') classNames.push(styles[props.type]);
    if (props.round) classNames.push(styles.round);
    if (props.disabled) classNames.push(styles.disabled);
    if (props.className) classNames.push(props.className);

    return <button type="button" className={classNames.join(' ')} onClick={props.onClick} style={props.style}>{props.children}</button>;
}

export default Button;