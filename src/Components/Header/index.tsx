import React from 'react';
import Title from '../Title';
import styles from './Header.module.css';

function Header() {
    return (
        <nav className={styles.navbar + ' box-shadow box-3'}>
            <Title size="h1" className={styles.icon}>OLS</Title>
            <Title size="h1">Ontology Webapp</Title>
        </nav>
    );
}

export default Header;
