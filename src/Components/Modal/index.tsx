import React from 'react';
import Button from '../Button';
import Container from '../Container';
import Title from '../Title';
import styles from './Modal.module.css';

interface ModalProps {
    id: string;
    term: OntologyTerm | null;
    onClose: () => void;
}

const Link: React.FC = (props) => <a href={props.children as string} rel="noreferrer" target="_blank">{props.children}</a>

function Modal(props: ModalProps) {
    if (!props.term) return <div hidden></div>;

    const item = props.term;

    return (
        <Container
            id={props.id}
            className={styles.backdrop}
            onClick={(e) => { if ((e.target as Element).id === props.id) props.onClose(); }}
        >
            <Container boxShadow={6} className={styles.modal}>
                <Title size="h2">{item.obo_id} Details</Title>
                <Button className={styles.close + ' box-shadow box-6'} round type="danger" onClick={() => props.onClose()}>&#9587;</Button>
            </Container>
            <Container boxShadow={6} className={styles.modal} style={{ paddingBottom: '2rem' }} >
                <Title style={{ paddingLeft: 0 }} size="h4">Label</Title>
                {item.label}
                <hr/>
                <Title style={{ paddingLeft: 0 }} size="h4">Description</Title>
                {item.description
                    ? item.description.map(s => <p style={{ margin: 0, marginBottom: '.5rem' }} key={s}>{s}</p>)
                    : 'No description'}
                <hr />
                <Title style={{ paddingLeft: 0 }} size="h4">Synonyms</Title>
                {item.synonyms.map(s => `'${s}'`).join(', ')}
                <hr/>
                <Title style={{ paddingLeft: 0 }} size="h4">Links</Title>
                <div>IRI: <Link>{item.iri}</Link></div>
                {item.annotation && item.annotation.exactMatch.length > 0 ? (
                    <div>
                        <div>Exact Matches:</div>
                        {item.annotation.exactMatch.map(link => <div>&nbsp;&nbsp;&nbsp;<Link>{link}</Link></div>)}
                    </div>
                ) : ''}
            </Container>
        </Container>
    );
}

export default Modal;
