import React from 'react';
import styles from './Search.module.css';

interface SearchProps {
    onChange: (value: string) => void;
    value: string;
    placeholder?: string;
    style?: React.CSSProperties;
}

function Search(props: SearchProps) {

    const id = `search-input-` + Math.round(Math.random() * 100000);

    return (
        <React.Fragment>
            <input
                placeholder={props.placeholder || 'Search'}
                id={id}
                className={styles.input}
                type="text"
                value={props.value}
                onChange={(e) => props.onChange(e.currentTarget.value)}
                style={props.style} />
        </React.Fragment>
    );
}

export default Search;
