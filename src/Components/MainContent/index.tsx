import React from 'react';
import Container from '../Container';

const MainContent: React.FC = (props) => (
    <Container style={{ padding: '2rem', flexGrow: 1, display: 'flex', justifyContent: 'center' }}>
        <Container style={{ maxWidth: 1000, flexGrow: 1 }}>
            {props.children}
        </Container>
    </Container>
);

export default MainContent;
