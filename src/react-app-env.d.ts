/// <reference types="react-scripts" />

interface OntologyLink {
    href: string;
}

interface OntologyTermsResponse {
    _embedded: {
        terms: OntologyTerm[];
    }
    page: {
        number: number;
        size: number;
        totalElements: number;
        totalPages: number;
    }
    _links: {
        first: OntologyLink;
        last: OntologyLink;
        next: OntologyLink;
        self: OntologyLink;
    }
}

interface OntologyTerm {
    iri: string;
    label: string;
    description: string[] | null;
    annotation: {
        database_cross_reference: string[];
        exactMatch: string[];
        id: string[];
        "term editor": string[];
    }
    synonyms: string[];
    ontology_name: string;
    ontology_prefix: string;
    ontology_iri: string;
    is_obsolete: boolean;
    term_replaced_by: string | null
    is_defining_ontology: boolean;
    has_children: boolean;
    is_root: boolean;
    short_form: string;
    obo_id: string;
    in_subset: boolean | null;
    obo_definition_citation: {
        definition: string;
        oboXrefs: {
            database: string;
            id: string;
            description: string | null;
            url: string | null;
        }[];
    }[];
    obo_synonym: {
        name: string;
        scope: string;
        type: string | null;
        xrefs: string[];
    }[];
    is_preferred_root: boolean
    _links: {
        self: OntologyLink;
        parents: OntologyLink;
        ancestors: OntologyLink;
        hierarchicalParents: OntologyLink;
        hierarchicalAncestors: OntologyLink;
        jstree: OntologyLink;
        graph: OntologyLink;
    }
}