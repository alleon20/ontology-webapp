import React from 'react';
import Header from '../Components/Header';
import MainContent from '../Components/MainContent';
import Table from '../Components/Table';

function App() {
    const dataSrcUrl = process.env.REACT_APP_ONTOLOGY_TERMS_URL || "https://www.ebi.ac.uk/ols/api/ontologies/efo/terms";

    return (
        <React.Fragment>
            <Header />
            <MainContent>
                <Table
                    title="Terms"
                    srcUrl={dataSrcUrl} />
            </MainContent>
        </React.Fragment>
    );
}

export default App;
