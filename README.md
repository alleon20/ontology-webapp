# Ontology Webapp

This project was created using [create-react-app](https://create-react-app.dev/)
and uses the material design logic (primary and secondary colors and paper design)

This project uses [this](https://www.ebi.ac.uk/ols/docs/api) open API and
displays the data from the
[Ontology EFO Terms](https://www.ebi.ac.uk/ols/api/ontologies/efo/terms).

The display method is a dynamic table with pagination and smart filtering.


## Build

First install nodejs, npm and yarn to install and build the project

```bash
npm install --global yarn
yarn install
```

If you wish to alter environment variables, create an `.env.local` file to the
project root directory and copy paste the contents of the `.env` file and then
change as you see fit.

The main variable to look out for should be `PUBLIC_URL`. If you plan on hosting
the project in `https://my-subdomain.example.com` then this variable should be
`PUBLIC_URL=`. If you plan to host it in `https://my-subdomain.example.com/my/url`,
it should be `PUBLIC_URL=/my/url`.

When you are ready for deployment, build the files.

```bash
yarn build
```

Finally, copy the files from the `build` directory the required hosting directory.


## Development

First install nodejs, npm and yarn.

```bash
npm install --global yarn
```

Open you favorite editor and change things as you see fit. To preview your changes, run:

```bash
yarn start
```

It should be noted that `react-scripts` library has hot-reload, so you do not need
to re-run the command to see your changes.

## Testing

To test the project run

```bash
yarn test
```

Currently a simple test has been created for the parent App component,
and it tests whether the rendered App contains the string `Ontology Webapp`

## License
[GPL-3.0](https://choosealicense.com/licenses/gpl-3.0)